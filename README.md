# GartenApp
This repository holds the code for the GartenApp (garden app), as described in the paper: 
Schneider, A.-K.; Strohbach, M.W.; App, M.; Schröder, B. 
The ‘GartenApp’: Assessing and Communicating the 
Ecological Potential of Private Gardens. Sustainability 2020, 12, 95. 
https://doi.org/10.3390/su12010095

It uses a PostgreSQL 9.6 server with the PostGIS 2.3.3 extension as a backend and a Shiny server v1.5.9.923 for hosting a RStudio Shiny app. 