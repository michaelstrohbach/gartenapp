---
geometry: left=2cm,right=2cm,top=2cm,bottom=2cm
header-includes:
- \usepackage{fontspec}
- \setmainfont{Latin Modern Sans}
- \usepackage[none]{hyphenat}
- \usepackage{multicol}
- \newcommand{\btwocol}{\begin{multicols}{2}}
- \newcommand{\etwocol}{\end{multicols}}
- \pagenumbering{gobble}
- \usepackage{fancyhdr}
- \pagestyle{fancy}
- \fancyhead{}
- \renewcommand{\headrulewidth}{0pt}
- \fancyfoot[LE,LO]{\includegraphics[height=1cm]{www/geo_lu_logo_rgb_dt.png}}
- \fancyfoot[RE,RO]{Die GartenApp wurden von Dr. Michael Strohbach und Dr. Anne-Kathrin Schneider erstellt.}
output:
  pdf_document:
    latex_engine: xelatex
  html_document:
    df_print: paged
params:
  carbStor: NA
  divisor: NA
  garden_area: NA
  het_elements: NA
  maxStor: NA
  rich: NA
  session_id: NA
  vh: NA
  s_a: NA
---

```{r setup, include=FALSE}

knitr::opts_chunk$set(echo = TRUE, results='asis')

```
  
# Danke, dass Sie bei der GartenApp mitgemacht haben! Hier ist Ihr Gartenbericht:
  
## Ihr Garten im Laserscan
\btwocol

```{r laserscan_figure, out.width='100%', echo = FALSE, fig.align='center'}
include_graphics(paste("images/",params$session_id,"laser_scan_test.png",sep=""))
```

```{r, include=FALSE}
grass <- round(sum(params$vh<0.5,na.rm=T)/length(params$vh)*100)
shrub <- round(sum(params$vh>=0.5 & params$vh<4, na.rm=T)/length(params$vh)*100)
tree  <- round(sum(params$vh>=4,na.rm=T)/length(params$vh)*100)

further_text <- if(grass>=15 & shrub>=15 & tree>=15){
   strong("Ihr Garten weist eine hohe Strukturvielfalt der Vegetation auf! Weiter so!")
  } else if (grass<15 & tree>=15 & shrub>=15) {
   strong("Wenn unter Ihren Bäumen eine Busch- und Krautschicht wächst, weist Ihr
          Garten eine hohe Strukturvielfalt auf. Weiter so!")         
  } else if(shrub<15 & tree >=15 & grass >=15){
   strong("Wenn unter Ihren Bäumen eine Busch- und Krautschicht wächst, weist Ihr
          Garten eine hohe Strukturvielfalt auf. Weiter so!")
  } else if (tree<15 & shrub>=15 & grass >=15){
   strong("Denken Sie daran: Mit dem Pflanzen von Bäumen können Sie die Vielfalt der 
          Vegetationsstruktur Ihres Gartens in den kommenden Jahren noch verbessern!")
  } else if (tree<15 & shrub<15 & grass >=15){
   strong("Bei Ihnen dominiert die Rasen- oder Grasfläche. Denken Sie daran: Mit dem Pflanzen von Büschen und Bäumen kann sich die Vegetationsstruktur Ihres Gartens in den kommenden Jahren noch verbessern!")
  } else if(tree<15 & shrub<15 & grass<15){
   strong("Denken Sie daran: Mit etwas mehr Gras, Büschen und Bäumen werden Sie bald einen viel strukturreicheren Garten haben!")
  } else if(tree>=15 & shrub<15 & grass<15){
   strong("Wenn unter Ihren Bäumen eine Busch- und Krautschicht wächst, weist Ihr Garten eine hohe Strukturvielfalt auf. Weiter so!")
  } else if(tree<15 & shrub>=15 & grass<15){
   strong("Bei Ihnen dominieren Büsche oder Hecken. Denken Sie daran: Ein paar Bäume und einige Gras- oder Wiesenflächen können die Vegetationsstruktur Ihres Gartens verbessern!")
  }
```

Gärten mit unterschiedlich hohen Pflanzen sind Struktur$\-$reich und strukturreiche Gärten bieten mehr Tierarten einen Lebens$\-$raum als strukturarme. Ihr Garten enthält $`r grass`$% niedrige (unter $\mbox{50 cm}$), $`r shrub`$% mittlere ($\mbox{0,5 m}$ bis $\mbox{4 m}$) 
und $`r tree`$% hohe Vegetation (größer $\mbox{4 m}$). 

`r further_text`

\etwocol

## Das 'leistet' Ihr Garten

\btwocol

### Unterstützung für Tiere und Pflanzen

```{r biodiv_figure, echo = FALSE, fig.height = 2, fig.width = 16, out.width="100%", fig.align="center"}
 max_bio <- 1
 min_s <- 4.74 # intercept
 max_s <- 4.74 + 0.08*c(length(params$het_elements)+2) + 0.17*3  # +2 for presence/absence of shrub and tree 
 het <- sum(params$het_elements)
 if(!is.na(any(params$vh>0.5 & params$vh<=4))) {het <- het + ifelse(any(params$vh>0.5 & params$vh<=4),1,0)} 
 if(!is.na(any(params$vh>4))) {het <- het + ifelse(any(params$vh>4),1,0)} 
 s <- ifelse(params$divisor>0,s <- min_s + 0.08*het + 0.17*params$rich, s <- min_s)
 s_rel <- 1-(max_s-s)/(max_s-min_s)

 if (length(s_rel)!=0){
   if(!is.na(s_rel)){
      par(mar=c(0.5,1,0.5,1), bg="transparent")

      if (s_rel<1){
       bp <- barplot(rbind(s_rel,c(1-s_rel)),
                     col=c("#d7301f","#fdcc8a"),horiz=TRUE, axes=F,
                     xlim=c(0-max_bio*0.1,max_bio+max_bio*0.1),
                     border=NA,ylim=c(-1,1))
       } else {
         par(mar=c(0.5,1,0.5,1), bg="transparent")
         bp <- barplot(s_rel,col=c("#d7301f"),
                       horiz=TRUE, axes=F,
                       xlim=c(0-max_bio*0.1,max_bio+max_bio*0.1),
                       border=NA,ylim=c(-1,1))
          }
        bp
     points(s_rel,-0.2,pch=17,cex=5,col="#bd0026")
  text(s_rel,-0.8,"Ihr Garten",col="#bd0026",font=2,cex=3)
  
  mtext(side=2,"wenig",las=2,cex=2.5,line=-5,font=2,at=0.7)
  mtext(side=4,"viel",las=2,cex=2.5,line=-5,font=2,at=0.7)
 }}
#include_graphics(paste("images/",params$session_id,"biodiv_barplot.png",sep=""))
```

```{r, include=F}
if(s_rel==0){
 biodiv_text <- paste("Sie können noch viel mehr tun, um den Artenreichtum Ihres Gartens zu erhöhen - schauen Sie doch nochmal in die Abfragen zu unterstützenden Maßnahmen zu Beginn der GartenApp.")
            } else if(s_rel>0 & s_rel<0.5){
 biodiv_text <- paste("Mit einigen zusätzlichen Maßnahmen können Sie den Artenreichtum Ihres Gartens erhöhen - schauen Sie doch nochmal in die Abfragen zu unterstützenden Maßnahmen zu Beginn der GartenApp.")
            } else if(s_rel>=0.5 & s_rel<0.8){
 biodiv_text <- paste("Mit ein paar kleinen zusätzlichen Maßnahmen können Sie den Artenreichtum Ihres Gartens erhöhen - schauen Sie doch nochmal in die Abfragen zu unterstützenden Maßnahmen zu Beginn der GartenApp.")
            } else if(s_rel>=0.8 & s_rel<1){
 biodiv_text <- paste("Sie haben schon viele Maßnahmen ergriffen, um Ihren Garten auch für Tiere und Pflanzen attraktiv zu gestalten. Wenn Sie  an weiteren Ideen interessiert sind, schauen Sie doch nochmal in die Abfragen zu unterstützenden Maßnahmen zu Beginn der GartenApp.")
            } else if(s_rel==1){
  biodiv_text <- paste("Dem haben wir nichts hinzuzufügen - Ihr Garten scheint sehr gut ausgestattet zu sein.")
            }
```

\vfill\columnbreak

`r biodiv_text`

\etwocol

\btwocol

### Kühlungspotenzial der Vegetation

```{r kuehl, echo = FALSE, fig.height = 2, fig.width = 16, out.width="100%", fig.align="center"}
# Cooling potential similarly to Reis and Lopes (2019), Sustainability
max_vegVol <- 3090 
max_lim <- 26.38-(26.38-0.0006*max_vegVol)
            
vegvol <- sum(params$vh,na.rm=T)/params$garden_area*309 
real_cool <- 26.38-(26.38-0.0006*vegvol)
if (real_cool>max_lim) {real_cool<-max_lim}
par(mar=c(0.5,1,0.5,1), bg="transparent")
barplot(rbind(real_cool,c(max_lim-real_cool)), 
              col=c("#08519c","#6baed6"),horiz=TRUE, axes=F, 
              xlim=c(0-max_lim*0.1,max_lim+max_lim*0.1),border=NA,ylim=c(-1,1))
points(real_cool,-0.2,pch=17,cex=3,col="#bd0026")
text(real_cool,-0.8,"Ihr Garten",col="#bd0026",font=2,cex=3)
mtext(side=2,"wenig",las=2,cex=2.5,line=-5,font=2,at=0.7)
mtext(side=4,"viel",las=2,cex=2.5,line=-5,font=2,at=0.7)

```

```{r, include=F}

cool_text <-  paste("Im Sommer verdunstet Vegetation Wasser und kühlt dadurch die Luft. Im Vergleich zu einer nicht bewachsenen Fläche in Braunschweig kann die Lufttemperatur in Ihrem Garten etwa ", round(real_cool,1),"°C darunter liegen.",sep="")

```

\vfill\columnbreak

`r cool_text`

\etwocol

\btwocol

### Schattenwurf durch Vegetation

```{r shadow, echo = FALSE, fig.height = 2, fig.width = 16, out.width="100%", fig.align="center"}
# Shadow

    s <- params$s_a/params$garden_area
    s_a <- round(params$s_a)
    garden_area <- round(params$garden_area)
    
    
    
    if(s==0){shadow_value <- 0} 
    if(s>0 & s<0.09){shadow_value <- 0.1} 
    if(s>=0.09 & s<0.15){shadow_value <- 0.2}
    if(s>=0.15 & s<0.28){shadow_value <- 0.3}
    if(s>=0.28 & s<0.35){shadow_value <- 0.4}
    if(s>=0.35 & s<0.48){shadow_value <- 0.5}     
    if(s>=0.48 & s<0.63){shadow_value <- 0.6}
    if(s>=0.63 & s<0.75){shadow_value <- 0.7}
    if(s>=0.75 & s<0.90){shadow_value <- 0.8}
    if(s>=0.90 & s<1.10){shadow_value <- 0.9}
    if(s>=1.10){shadow_value <- 1}
    
    par(mar=c(0.5,1,0.5,1), bg="transparent")
    bp <- barplot(rbind(shadow_value, 1-shadow_value), 
                  col=c("#737373","#d9d9d9"),horiz=TRUE, axes=F, 
                  xlim=c(-0.1,1.1),border=NA,ylim=c(-1,1))
    
    points(shadow_value,-0.2,pch=17,cex=3,col="#bd0026")
    text(shadow_value,-0.8,"Ihr Garten",col="#bd0026",font=2,cex=3)
    
    mtext(side=2,"wenig",las=2,cex=2.5,line=-5,font=2,at=0.7)
    mtext(side=4,"viel",las=2,cex=2.5,line=-5,font=2,at=0.7)
    

```



\vfill\columnbreak

"Durch Schattenwurf tragen Sträucher und Bäume zur Kühlung bei, denn darunterliegende Oberflächen heizen sich nicht so stark auf. Die Vegetation in Ihrem Garten beschattet z.B. im Hochsommer am Mittag ", $`r s_a`$ , " Quadratmeter. Das entspricht ungefähr ", $`r round(s*100)`$, "% der Gartenfläche."

\etwocol

\btwocol

### Kohlenstoffspeicherung in der Vegetation

```{r carbon, echo = FALSE, fig.height = 2, fig.width = 16, out.width="100%", fig.align="center"}
carbon_text <- params$carbStor
par(mar=c(0.5,1,0.5,1), bg="transparent")
 if(params$carbStor > .67*params$maxStor){
   plot_color <- "#3182bd"
  } else if(params$carbStor < .34*params$maxStor){
   plot_color <- "#deebf7"
  } else if(params$carbStor >= .34*params$maxStor & params$carbStor <= .67*params$maxStor){
   plot_color <- "#9ecae1"
  }      
  barplot(rbind(params$carbStor,c(params$maxStor-params$carbStor)), 
                col=c('#006837','#b8e186'),horiz=TRUE, axes=F, 
                xlim=c(0-params$maxStor*0.1,params$maxStor+params$maxStor*0.1),border=NA,ylim=c(-1,1))
  points(params$carbStor,-0.2,pch=17,cex=5,col="#bd0026")
  text(params$carbStor,-0.8,"Ihr Garten",col="#bd0026",font=2,cex=3)
  
  mtext(side=2,"wenig",las=2,cex=2.5,line=-5,font=2,at=0.7)
  mtext(side=4,"viel",las=2,cex=2.5,line=-5,font=2,at=0.7)
```

\vfill\columnbreak

In Ihrem Garten sind ca. $`r as.character(round(carbon_text,0))`$ kg Kohlenstoff festgelegt. Das entspricht der
Menge Kohlenstoffdioxid, die ein PKW (Benziner) auf einer Strecke von 
$`r as.character(round(carbon_text*(44/12)/0.168,0))`$ km ausstößt.

\etwocol

\pagebreak

## Durchlässigkeit Ihres Gartens und der Umgebung für Wildtiere
\btwocol

```{r connectivity_figure, out.width='75%', echo = FALSE}
include_graphics(paste("images/",params$session_id,"connectivity_plot.png",sep=""))
```

\vfill\columnbreak

Hier sehen Sie die Wichtigkeit des Gartens als Bewegungs$\-$korridor für Eichhörnchen. Zeigt Ihr Garten orange oder gelbe Farben, ist er vermutlich wichtig, damit Eichhörnchen von A nach B kommen. Eichhörnchen sind charakteristisch für Wildtiere in der Stadt, die auf Bäume angewiesen sind. 

\etwocol

\btwocol

```{r connectivity_igel_figure, out.width='75%', echo = FALSE}
include_graphics(paste("images/",params$session_id,"connectivity_plot_igel.png",sep=""))
```

\vfill\columnbreak


Hier sehen Sie die Wichtigkeit des Gartens als Bewegungs$\-$korridor für Igel. Zeigt Ihr Garten orange oder gelbe Farben, ist er vermutlich wichtig, damit Igel von A nach B kommen\*. Igel sind charakteristisch für Wildtiere in der Stadt, die auf Rasenflächen, Wiesen und Gebüsche angewiesen sind. \
\*Bei unserem Modell sind wir davon ausgegangen, dass Kleingärten und Friedhöfe zum Schutz vor Wildkaninchen eingezäunt und somit auch nicht für Igel zugänglich sind. </p>

\etwocol
